/*
 * Copyright (c) 2016 w-innovations. All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.winnovations.jak.gradle;

import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ModelBuilder;
import org.gradle.tooling.ProjectConnection;
import org.gradle.tooling.model.ExternalDependency;
import org.gradle.tooling.model.GradleModuleVersion;
import org.gradle.tooling.model.idea.IdeaModule;
import org.gradle.tooling.model.idea.IdeaProject;
import org.gradle.tooling.model.idea.IdeaSingleEntryLibraryDependency;

import java.io.File;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DefaultModuleVersionResolver implements ModuleVersionResolver {
    private final Function<File, ProjectConnection> projectConnectionResolver;

    public DefaultModuleVersionResolver() {
        this((projectRoot) ->
                GradleConnector.newConnector()
                        .forProjectDirectory(projectRoot)
                        .connect());
    }

    public DefaultModuleVersionResolver(Function<File, ProjectConnection> projectConnectionResolver) {
        this.projectConnectionResolver = projectConnectionResolver;
    }

    @Override
    public List<GradleModuleVersion> findModuleVersions(File projectRoot) {
        ProjectConnection connection = projectConnectionResolver.apply(projectRoot);
        ModelBuilder<IdeaProject> ideaProjectBuilder = connection.model(IdeaProject.class);
        ideaProjectBuilder.setJvmArguments("-D" + JakWorkspace.RECURSIVE_PROPERTY + "=true");
        ideaProjectBuilder.setStandardOutput(System.out);
        ideaProjectBuilder.setStandardError(System.err);
        IdeaProject ideaProject = ideaProjectBuilder.get();
        return ideaProject.getModules().stream()
                .map(IdeaModule::getDependencies)
                .flatMap(d -> d.getAll().stream())
                .filter(d -> d instanceof IdeaSingleEntryLibraryDependency)
                .map(d -> (IdeaSingleEntryLibraryDependency) d)
                .map(ExternalDependency::getGradleModuleVersion)
                .collect(Collectors.toList());

    }
}
