/*
 * Copyright (c) 2016 w-innovations. All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.winnovations.jak.gradle;

import com.winnovations.jak.gradle.workspace.Workspace;
import com.winnovations.jak.gradle.workspace.WorkspacePackage;
import lombok.extern.slf4j.Slf4j;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.util.List;
import java.util.Optional;

@Slf4j
public class ProjectPlugin implements Plugin<Project> {
    private final WorkspaceResolver workspaceResolver;
    private final PackageMatcher packageMatcher;

    public ProjectPlugin() {
        this(new DefaultWorkspaceResolver(), new DefaultPackageMatcher());
    }

    public ProjectPlugin(WorkspaceResolver workspaceResolver, PackageMatcher packageMatcher) {
        this.workspaceResolver = workspaceResolver;
        this.packageMatcher = packageMatcher;
    }

    @Override
    public void apply(Project project) {
        Optional<Workspace> workspace = workspaceResolver.findWorkspace(project.getRootDir());
        if (!workspace.isPresent()) {
            log.warn("jak.workspace not found. Skipping workspace resolution.");
        }
        project.afterEvaluate(p -> afterEvaluate(p, workspace.get()));
    }

    private void afterEvaluate(Project project, Workspace workspace) {
        List<WorkspacePackage> workspacePackages = workspace.getPackages();
        project.getConfigurations().stream().forEach(config -> {

            config.getAllDependencies().forEach(dependency -> {
                Optional<WorkspacePackage> wp = packageMatcher.matchPackageToDependency(workspacePackages, dependency);
                if (!wp.isPresent()) {
                    return;
                }
                config.getResolutionStrategy().dependencySubstitution(dependencySubstitutions ->
                        dependencySubstitutions
                                .substitute(dependencySubstitutions.module(String.format("%s:%s:%s", dependency.getGroup(), dependency.getName(), dependency.getVersion())))
                                .with(dependencySubstitutions.project(wp.get().getProjectName()))
                );
            });
        });
    }
}
