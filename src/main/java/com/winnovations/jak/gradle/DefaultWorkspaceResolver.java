/*
 * Copyright (c) 2016 w-innovations. All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.winnovations.jak.gradle;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.winnovations.jak.gradle.workspace.Workspace;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class DefaultWorkspaceResolver implements WorkspaceResolver {
    private ObjectMapper objectMapper;

    public DefaultWorkspaceResolver() {
        this(new ObjectMapper());
    }

    public DefaultWorkspaceResolver(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<Workspace> findWorkspace(File root) {
        File workspaceDir = root.getParentFile().getParentFile();
        File[] files = workspaceDir.listFiles((dir, name) -> name.equals("jak.workspace"));
        if (files.length == 0) {
            return Optional.empty();
        } else if (files.length > 1) {
            String fileNames = Arrays.asList(files).stream().map(File::getName).collect(Collectors.joining("\n"));
            log.error("Multiple jak.workspace files found. This is an unexpected error. Please open an issue with us!\n" +
                    "The following files were found: \n" + fileNames);
            throw new RuntimeException("Multiple jak.workspace files found. Please contact us.");
        }

        try {
            return Optional.of(objectMapper.readValue(files[0], Workspace.class));
        } catch (IOException e) {
            throw new RuntimeException("Unable to parse jak.workspace file.", e);
        }
    }
}
