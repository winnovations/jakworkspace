/*
 * Copyright (c) 2016 w-innovations. All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.winnovations.jak.gradle;

import com.winnovations.jak.gradle.workspace.WorkspacePackage;
import org.gradle.api.artifacts.Dependency;
import org.gradle.tooling.model.GradleModuleVersion;

import java.util.Collection;
import java.util.Optional;

public class DefaultPackageMatcher implements PackageMatcher {
    @Override
    public Optional<WorkspacePackage> matchPackageToVersion(Collection<WorkspacePackage> packages, GradleModuleVersion version) {
        if (version == null) {
            return Optional.empty();
        }
        return matches(packages, version.getGroup(), version.getName(), version.getVersion());
    }

    @Override
    public Optional<WorkspacePackage> matchPackageToDependency(Collection<WorkspacePackage> packages, Dependency dependency) {
        if (dependency == null) {
            return Optional.empty();
        }
        return matches(packages, dependency.getGroup(), dependency.getName(), dependency.getVersion());
    }

    private Optional<WorkspacePackage> matches(Collection<WorkspacePackage> packages,
                            String group,
                            String name,
                            String version) {
        return packages.stream()
                .filter(p ->  p.getGroup().equals(group) &&
                        p.getName().equals(name) &&
                        p.getVersion().equals(version))
                .findFirst();
    }
}
