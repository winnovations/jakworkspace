/*
 * Copyright (c) 2016 w-innovations. All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.winnovations.jak.gradle.workspace;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;
import org.gradle.tooling.model.GradleModuleVersion;

@Value
@Builder
@JsonDeserialize(builder = WorkspacePackage.WorkspacePackageBuilder.class)
public class WorkspacePackage {
    String group;
    String name;
    String version;
    String path;

    public String getProjectName() {
        return ":" + name;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class WorkspacePackageBuilder {}
}
