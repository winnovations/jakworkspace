/*
 * Copyright (c) 2016 w-innovations. All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.winnovations.jak.gradle;

import com.winnovations.jak.gradle.workspace.Workspace;
import com.winnovations.jak.gradle.workspace.WorkspacePackage;
import lombok.extern.slf4j.Slf4j;
import org.gradle.api.Plugin;
import org.gradle.api.initialization.Settings;
import org.gradle.tooling.model.GradleModuleVersion;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

@Slf4j
public class SettingsPlugin implements Plugin<Settings> {
    private final WorkspaceResolver workspaceResolver;
    private final ModuleVersionResolver moduleVersionResolver;
    private final PackageMatcher packageMatcher;

    public SettingsPlugin() {
        this(new DefaultWorkspaceResolver(), new DefaultModuleVersionResolver(), new DefaultPackageMatcher());
    }

    public SettingsPlugin(WorkspaceResolver workspaceResolver,
                          ModuleVersionResolver moduleVersionResolver,
                          PackageMatcher packageMatcher) {
        this.workspaceResolver = workspaceResolver;
        this.moduleVersionResolver = moduleVersionResolver;
        this.packageMatcher = packageMatcher;
    }

    @Override
    public void apply(Settings settings) {
        Optional<Workspace> workspace = workspaceResolver.findWorkspace(settings.getRootDir());

        if (!workspace.isPresent()) {
            log.warn("jak.workspace not found. Skipping workspace resolution.");
        }

        Map<GradleModuleVersion, WorkspacePackage> packageVersions = findDependentLocalPackageVersions(settings.getRootDir(), workspace.get());
        packageVersions.forEach((moduleVersion, pkg) -> {
            settings.include(new String[]{pkg.getProjectName()});
            settings.project(pkg.getProjectName()).setProjectDir(new File(settings.getRootDir().getParentFile().getParentFile(), pkg.getPath()));
        });
    }

    private Map<GradleModuleVersion, WorkspacePackage> findDependentLocalPackageVersions(File packageRoot,
                                                                                         Workspace workspace) {
        List<GradleModuleVersion> rootModuleVersions = moduleVersionResolver.findModuleVersions(packageRoot);

        Map<GradleModuleVersion, WorkspacePackage> localPackageMap = new HashMap<>();

        for(GradleModuleVersion moduleVersion: rootModuleVersions) {
            Optional<WorkspacePackage> workspacePackage = packageMatcher.matchPackageToVersion(workspace.getPackages(), moduleVersion);
            workspacePackage
                    .filter(excludeSameProjectPredicate(packageRoot))
                    .ifPresent(p -> localPackageMap.put(moduleVersion, p));
        }

        return localPackageMap;
    }

    private Predicate<WorkspacePackage> excludeSameProjectPredicate(File packageRoot) {
        return p -> {
            Path path = Paths.get(packageRoot.getParentFile().getParentFile().toPath().toString(), p.getPath());
            try {
                return !Files.isSameFile(path, packageRoot.toPath());
            } catch (IOException e) {
                throw new RuntimeException("Unable to evaluate if package and project directories are equal.", e);
            }
        };
    }
}
