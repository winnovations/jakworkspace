/*
 * Copyright (c) 2016 w-innovations. All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.winnovations.jak.gradle;

import lombok.extern.slf4j.Slf4j;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.initialization.Settings;

import javax.inject.Inject;

@Slf4j
public class JakWorkspace implements Plugin {
    static final String RECURSIVE_PROPERTY = "jakRecursive";
    private final SettingsPlugin settingsPlugin;
    private final ProjectPlugin projectPlugin;

    @Inject
    public JakWorkspace() {
        this(new SettingsPlugin(), new ProjectPlugin());
    }

    JakWorkspace(SettingsPlugin settingsPlugin, ProjectPlugin projectPlugin) {
        this.settingsPlugin = settingsPlugin;
        this.projectPlugin = projectPlugin;
    }

    @Override
    public void apply(Object target) {
        if (System.getProperty(JakWorkspace.RECURSIVE_PROPERTY) != null) {
            log.debug("jak recursive property detected. Skipping workspace resolution.");
            return;
        }

        if (target instanceof Settings) {
            settingsPlugin.apply((Settings) target);
        } else if (target instanceof Project) {
            projectPlugin.apply((Project) target);
        } else {
            log.info("Unable to apply jak-workspace plugin to " + target.getClass().getName());
        }
    }
}
